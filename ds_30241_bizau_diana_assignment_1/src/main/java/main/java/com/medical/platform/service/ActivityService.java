package main.java.com.medical.platform.service;

import main.java.com.medical.platform.model.Activity;
import main.java.com.medical.platform.repos.ActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ActivityService {
    @Autowired
    ActivityRepo activityRepo;

    public Activity save(Long id, Date start, Date end, String activity) {
        Activity act = activityRepo.save(new Activity(id,activity,start,end));
        return act;
    }
}
