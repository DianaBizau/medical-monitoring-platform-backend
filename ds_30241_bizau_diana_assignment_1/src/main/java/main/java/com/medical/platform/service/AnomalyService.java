package main.java.com.medical.platform.service;

import main.java.com.medical.platform.model.Anomaly;
import main.java.com.medical.platform.repos.AnomalyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnomalyService {
    @Autowired
    AnomalyRepo anomalyRepo;

    public Anomaly save (Anomaly anomaly)
    {

        Anomaly an=anomalyRepo.save(anomaly);
        return an;
    }


}
