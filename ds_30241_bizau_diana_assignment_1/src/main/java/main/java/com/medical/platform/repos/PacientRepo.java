package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Pacient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface PacientRepo extends CrudRepository<Pacient,Long> {
    Pacient findByUsername(String username);
    Pacient findById(Long id);
}
