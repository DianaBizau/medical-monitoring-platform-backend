package main.java.com.medical.platform.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;


@RestController
public class RabbitMQWebController {
/*
	@GetMapping(value = "/producer")
	public String producer(@RequestParam("start") Long start,@RequestParam("end") Long end) {

	Activity emp=new Activity();
	emp.setStart(start);
	emp.setEnd(end);
		rabbitMQSender.sendActivity(emp);

		return "Message sent to the RabbitMQ Successfully";
	}*/
    @Autowired
    Producer producer;

    @RequestMapping("/send")
    public String sendMsg() throws IOException, ParseException {
        producer.produceMsg();
        return "Done";
    }





}