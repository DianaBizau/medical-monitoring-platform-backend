package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Anomaly;
import org.springframework.data.repository.CrudRepository;

public interface AnomalyRepo  extends CrudRepository<Anomaly,Long> {
}
