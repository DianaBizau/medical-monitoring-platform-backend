package main.java.com.medical.platform.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
@Entity
public class Doctor{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gen;
    private LocalDateTime date;
    private String rol;
    private String address;
    private String username;
    private String password;
    @JsonIgnore
    @OneToMany
    @JoinColumn(name="idDoctor")
    private List<PMedication> medPlan;
    Doctor()
    {

    }

    public Doctor(String name, String gen, LocalDateTime date, String rol, String address, String username, String password, List<PMedication> medPlan) {
        this.name = name;
        this.gen = gen;
        this.date = date;
        this.rol = rol;
        this.address = address;
        this.username = username;
        this.password = password;
        this.medPlan = medPlan;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<PMedication> getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(List<PMedication> medPlan) {
        this.medPlan = medPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(getId(), doctor.getId()) &&
                Objects.equals(getName(), doctor.getName()) &&
                Objects.equals(getGen(), doctor.getGen()) &&
                Objects.equals(getDate(), doctor.getDate()) &&
                Objects.equals(getRol(), doctor.getRol()) &&
                Objects.equals(getAddress(), doctor.getAddress()) &&
                Objects.equals(getUsername(), doctor.getUsername()) &&
                Objects.equals(getPassword(), doctor.getPassword()) &&
                Objects.equals(getMedPlan(), doctor.getMedPlan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getGen(), getDate(), getRol(), getAddress(), getUsername(), getPassword(), getMedPlan());
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gen='" + gen + '\'' +
                ", date=" + date +
                ", rol='" + rol + '\'' +
                ", address='" + address + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", medPlan=" + medPlan +
                '}';
    }
}
