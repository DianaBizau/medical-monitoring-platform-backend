package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Activity;
import org.springframework.data.repository.CrudRepository;

public interface ActivityRepo extends CrudRepository<Activity,Long> {
}
