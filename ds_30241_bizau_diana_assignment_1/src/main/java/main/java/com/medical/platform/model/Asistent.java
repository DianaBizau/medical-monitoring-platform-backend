package main.java.com.medical.platform.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Entity
public class Asistent{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gen;
    private String  date;
    private String rol;
    private String address;
    private String username;
    private String password;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="idAsistent")
    private List<Pacient> pacienti=new ArrayList<>();

    public Asistent()
    {

    }

    public Asistent(String name, String gen, String  date, String rol, String address, String username, String password, List<Pacient> pacienti) {
        this.name = name;
        this.gen = gen;
        this.date = date;
        this.rol = rol;
        this.address = address;
        this.username = username;
        this.password = password;
        this.pacienti = pacienti;
    }
    public List<Pacient> addPacient(Pacient p) {
        this.pacienti.add(p);
        return this.pacienti;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Pacient> getPacienti() {
        return pacienti;
    }

    public void setPacienti(List<Pacient> pacienti) {
        this.pacienti = pacienti;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Asistent)) return false;
        Asistent asistent = (Asistent) o;
        return Objects.equals(getPacienti(), asistent.getPacienti());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPacienti());
    }

    @Override
    public String toString() {
        return "Asistent{" +
                "pacienti=" + pacienti +
                '}';
    }
}
