package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Plan;
import org.springframework.data.repository.CrudRepository;

import javax.sql.rowset.CachedRowSet;

public interface PlanRepo extends CrudRepository<Plan,Integer> {
}
