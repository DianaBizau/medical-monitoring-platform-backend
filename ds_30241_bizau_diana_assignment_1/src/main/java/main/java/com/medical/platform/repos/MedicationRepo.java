package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface MedicationRepo extends CrudRepository<Medication,Long> {
}
