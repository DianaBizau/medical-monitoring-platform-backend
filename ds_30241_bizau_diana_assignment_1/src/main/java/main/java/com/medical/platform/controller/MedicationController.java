package main.java.com.medical.platform.controller;

import main.java.com.medical.platform.model.Medication;
import main.java.com.medical.platform.repos.MedicationRepo;
import main.java.com.medical.platform.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/medication")
public class MedicationController {

    @Autowired
    private MedicationService medicationService;
    @GetMapping("/findAll")

    public List<Medication> findAll(Medication medication){
        return medicationService.findAll();
    }
}
