package main.java.com.medical.platform.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class Producer {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingkey}")
    private String routingKey;


    void produceMsg() throws IOException, ParseException {

        int iteratii = 0;
        Random random = new Random();
        int max = 3, min = 1;

        File file = new File("C:\\Users\\Diana\\Desktop\\ds_30241_bizau_diana_assignment_1\\activity.txt");

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String st;
        while ((st = br.readLine()) != null) {
            iteratii++;
            int i = random.nextInt((max - min) + 1) + min;
            st = i + "		" + st;
            amqpTemplate.convertAndSend(exchange, routingKey, st);
            if (iteratii < 10) {
                System.out.println("Activity 0" + iteratii + " = " + st);
            } else {
                System.out.println("Activity " + iteratii + " = " + st);
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (iteratii > 99) {
                break;
            }
        }
    }

//   private String convertToJson(String st, long id) throws JsonProcessingException, ParseException {
//        String[] split = st.split("\t\t");
//
//        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date start=formatter.parse(split[0]);
//        Date end=formatter.parse(split[1]);
//        Activity activity = new Activity((id+1),split[2],start,end);
//
//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//
//        return ow.writeValueAsString(activity);
//
//    }
}
