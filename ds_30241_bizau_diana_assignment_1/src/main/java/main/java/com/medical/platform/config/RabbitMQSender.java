package main.java.com.medical.platform.config;

import main.java.com.medical.platform.model.Activity;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSender {
	
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${javainuse.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${javainuse.rabbitmq.routingkey}")
	private String routingkey;	
	

	public void sendActivity(Activity activity){
		rabbitTemplate.convertAndSend(exchange,routingkey,activity);
		System.out.println("Send msg= "+activity);
	}
}