package main.java.com.medical.platform.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.medical.platform.model.*;
import main.java.com.medical.platform.repos.AsistentRepo;
import main.java.com.medical.platform.repos.DoctorRepo;
import main.java.com.medical.platform.repos.PacientRepo;
import main.java.com.medical.platform.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private DoctorRepo doctorRepo;
    @Autowired
    private AsistentRepo asistentRepo;
    @Autowired
    private ObjectMapper mapper;

    @GetMapping("/findAll")
    public List<Doctor> findAll(Doctor doctor){
        return doctorService.findAll();
    }
    @Autowired
    private PacientRepo pacientRepository;


    @DeleteMapping("/delete/{id}")
    ResponseEntity<Asistent> deleteAsistent(Long id)
    {
        asistentRepo.delete(id);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/remove/{id}")
    ResponseEntity<Pacient> remove(@PathVariable Long id)
    {
        pacientRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @PostMapping("/createPacient")
    ResponseEntity<Pacient> addPacient(@Valid @RequestBody Pacient pacient) throws URISyntaxException {
        Pacient result=pacientRepository.save(pacient);
        return ResponseEntity.created(new URI("/doctor/createPacient"+result.getId())).body(result);
    }
    @PostMapping("/createAsistent")
    ResponseEntity<Asistent> addAsistent(@Valid @RequestBody Asistent asistent) throws URISyntaxException {
        Asistent result=asistentRepo.save(asistent);
        return ResponseEntity.created(new URI("/doctor/createAsistent"+result.getId())).body(result);
    }

    @PostMapping("/addAsistentToPatient")
    public ResponseEntity<Asistent> addCaregiverToPatient( @RequestBody Map<String, Object> data) {
        Asistent c = mapper.convertValue(data.get("asistent"), Asistent.class);
        Pacient p = mapper.convertValue(data.get("pacient"), Pacient.class);
        doctorService.addPatientToCaregiver(p.getId(), c.getId());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/addMedicationToPatient")
    public ResponseEntity<PMedication> addMedicationPlanToPatient(Principal user, @RequestBody Map<String, Object> data) {
        Doctor d =mapper.convertValue(data.get("doctor"),Doctor.class);
        Pacient p = mapper.convertValue(data.get("pacient"), Pacient.class);
        Medication m = mapper.convertValue(data.get("medication"), Medication.class);
        doctorService.addMedicationToPatient(d, p, m, data.get("intervalHours").toString(), data.get("period").toString());
        return ResponseEntity.ok().build();
    }



}
